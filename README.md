Kivish
======

Kivish is a simple html template language, which could be an alternative to the Jade or Haml.<br/>
Please visit [the website](http://kivish.vipserv.org) to read more about it.

Install
-------
Python 3 is required for the compiler to run.

You can install kivish by pip: `pip3 install kivish`

Or manually, using setup.py (setup tools package required): `python3 setup.py install`

Contribution
------------
Please insert your suggestions and bug reports at the issues tab.<br/>
You can also contact me directly: jakub.jaroslaw.ligeza at gmail.com

Tests
-----
Kivish is tested with py.test. Each internal package has its own test set.<br/>
To execute all test, type `py.test` in the root directory.

Lincense
--------
MIT

TODO
----
- [ ] passing arguments to rules
- [ ] import statement
- [ ] automatic kvs files discovery and compilation
- [ ] indentation options
