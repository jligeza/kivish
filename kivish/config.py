#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-
config = {
    'INPUT_INDENT': 4,
    'OUTPUT_INDENT': 2
}
