#!/usr/bin/env python3.5
# -*- coding: utf-8 -*-
from sys import argv
import kivish


def main():
    target = ''

    try:
        target = argv[1]
    except:
        return print('Must specify target .kvs file.')

    kivish.compile(target)


if __name__ == "__main__":
    main()
